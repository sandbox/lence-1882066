<?php

/**
 * @file
 *  Creates a timepicker field widget.
 *  Based on the examples/field_example and cck_time modules.
 *
 *  Requires the 3th party jQuery UI Timepicker library:
 *  http://fgelinas.com/code/timepicker/
 *
 *  Simply put jquery.ui.timepicker.css and jquery.ui.timepicker.js in
 *  /sites/all/libraries/timepicker/
 *  and make sure the Libraries API module is installed.
 */

/**
 * Implements hook_field_info().
 */
function field_timepicker_field_info() {
  return array(
    'field_timepicker' => array(
      'label' => t('Timepicker Field'),
      'description' => t('Store a time of day.'),
      'default_widget' => 'field_timepicker_select',
      'default_formatter' => 'field_timepicker_default',
    ),
  );
}

/**
 * Implements hook_field_validate().
 *
 * @see field_timepicker_field_widget_error()
 */
function field_timepicker_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  foreach ($items as $delta => $item) {
    if (!empty($item['timepickervalue'])) {
      if (! preg_match('/^([0-1][1-9]|2[0-3])[:]([0-5][0-9])$/', $item['timepickervalue'])) {
        $errors[$field['field_name']][$langcode][$delta][] = array(
          'error' => 'field_timepicker_invalid',
          'message' => t('Time must be in the valid hh:mm (24-hour) format.'),
        );
      }
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function field_timepicker_field_is_empty($item, $field) {
  return empty($item['timepickervalue']);
}

/**
 * Implements hook_field_formatter_info().
 */
function field_timepicker_field_formatter_info() {
  return array(
    'field_timepicker_default' => array(
      'label' => t('Timepicker formatter'),
      'field types' => array('field_timepicker'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function field_timepicker_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  switch ($display['type']) {
    case 'field_timepicker_default':
      foreach ($items as $delta => $item) {
          $element[$delta] = array(
            '#type' => 'html_tag',
            '#tag' => 'span',
            '#value' => t('@timepickervalue', array('@timepickervalue' => $item['timepickervalue'])),
          );
      }
      break;
  }
  return $element;
}

/**
 * Implements hook_field_widget_info().
 */
function field_timepicker_field_widget_info() {
  return array(
    'field_timepicker_select' => array(
      'label' => t('Timepicker Widget'),
      'field types' => array('field_timepicker'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function field_timepicker_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $value = isset($items[$delta]['timepickervalue']) ? $items[$delta]['timepickervalue'] : '';

  $widget = $element;
  $widget['#delta'] = $delta;

  switch ($instance['widget']['type']) {

    case 'field_timepicker_select':

      if (($library = libraries_detect('timepicker')) && !empty($library['installed'])) {
        libraries_load('timepicker');
        $widget += array(
          '#attributes' => array('class' => array('field-timepicker')),
          '#attached' => array(
            'library' => array(
              array('system', 'ui.accordion', 'ui.core', 'ui.widget', 'ui.tabs', 'ui.position'),
            ),
            'css' => array(
            drupal_get_path('module', 'field_timepicker') . '/field_timepicker.css',
            ),
            'js' => array(
              drupal_get_path('module', 'field_timepicker') . '/field_timepicker.js',
            ),
          ),
        );
      }
      else {
        $error = $library['error'];
        $error_message = $library['error message'];
      }
      $widget += array(
        '#type' => 'textfield',
        '#default_value' => $value,
        '#size' => 5,
        '#maxlength' => 5,
      );
      break;
  }

  $element['timepickervalue'] = $widget;
  return $element;
}

/**
 * Implements hook_field_widget_error().
 *
 * @see field_timepicker_field_validate()
 * @see form_error()
 */
function field_timepicker_field_widget_error($element, $error, $form, &$form_state) {
  switch ($error['error']) {
    case 'field_timepicker_invalid':
      form_error($element, $error['message']);
      break;
  }
}

/**
 * Implements hook_libraries_info().
 */
function field_timepicker_libraries_info() {
  $libraries['timepicker'] = array(
    'name' => 'jQuery UI Timepicker',
    'vendor url' => 'http://fgelinas.com/code/timepicker/',
    'download url' => 'http://fgelinas.com/code/timepicker/',
    'version arguments' => array(
      'file' => 'jquery.ui.timepicker.js',
      'pattern' => '@jQuery UI Timepicker ([0-9\.a-z]+)@',
      'lines' => 2,
    ),
    'files' => array(
      'js' => array(
        'jquery.ui.timepicker.js',
      ),
      'css' => array(
        'jquery.ui.timepicker.css',
      ),
    ),
  );
  return $libraries;
}
