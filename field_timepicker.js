/**
 * @file
 * Javascript for Field Timepicker.
 */

// Nederlands initialisation for the timepicker plugin.
/*(function($) {
  $.timepicker.regional['nl'] = {
    hourText: 'Uren',
    minuteText: 'Minuten',
    closeButtonText: 'Sluiten',
    nowButtonText: 'Actuele tijd',
    deselectButtonText: 'Wissen'
  }
  $.timepicker.setDefaults($.timepicker.regional['nl']);
})(jQuery);*/

(function($) {
  $(document).ready(function() {
    $('.field-timepicker').timepicker();
  });
})(jQuery);
